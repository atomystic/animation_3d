let scene, camera, renderer, light, cube, plane, light2, sun
let cubes = [];
let stars = []
let yEndPositionCube = 0;
let yStartPositionCube = 20;
let AddY = 0.3;
let AddTheta = 0.062;
let startheta = 0;
let Endtheta = 0.2;
let RADIUS = 10;
let yCameraEnd = 0
let lookAt = new THREE.Vector3(0, 0, 0);
let lookAtEndY;
let RADIUS_SUN = 1000;
let thetaSun = 0;

let turnAndUp = (e) => {
    createGeometry();
}

let createGeometry = () => {
    let geometry = new THREE.BoxGeometry(5, 5, 5);
    let material = new THREE.MeshPhongMaterial({
        color: 0x000000,
        side: THREE.DoubleSide,
        shininess: 100
    })
    cube = new THREE.Mesh(geometry, material);
    cube.position.y = yStartPositionCube;

    yStartPositionCube += 5;
    yEndPositionCube += 5;
    Endtheta += Math.PI
    yCameraEnd += 5;
    lookAtEndY = lookAt.y + 5;
    scene.add(cube)

}

let createPlane = () => {
    let geometry = new THREE.BoxGeometry(100, 0.1, 100);
    let material = new THREE.MeshPhongMaterial({
        color: 0x8b4513,
        side: THREE.DoubleSide,
        shininess: 100
    })
    plane = new THREE.Mesh(geometry, material);
    plane.position.x = -1
    scene.add(plane)

}

let randomInRange = (from, to) => {
    random = Math.random() * (to - from)
    return (random + from);
}
let createStar = () => {
    let geometry = new THREE.SphereGeometry(randomInRange(0.3, 0.7), randomInRange(8, 40), randomInRange(8, 40));
    for (let i = 0; i < 5000; i++) {
        let material = new THREE.MeshPhongMaterial({
            color: 0xffffff,
            side: THREE.DoubleSide,
            shininess: 100

        })

        let etoile = new THREE.Mesh(geometry, material);
        etoile.position.set(randomInRange(-500, 500), randomInRange(-500, 500), randomInRange(-500, 500))

        stars.push(etoile)
        scene.add(etoile)
    }
}

let createSun = () => {
    let geometry = new THREE.SphereGeometry(200, 30, 30);
    let material = new THREE.MeshPhongMaterial({
        color: 0xfff400,
        side: THREE.DoubleSide,
        shininess: 100

    })

    sun = new THREE.Mesh(geometry, material);
    sun.position.set(RADIUS_SUN, 20, 0)

    scene.add(sun)

}


let init = () => {
    scene = new THREE.Scene();
    scene.background = new THREE.Color(0x000066);

    camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 1, 1000)
    camera.position.set(10, 0, RADIUS); //(3, 0, 13);

    // light = new THREE.DirectionalLight(0xffffff, 1)
    light2 = new THREE.AmbientLight(0xffffff, 1)
    light = new THREE.SpotLight(0xffffff, 1.5)
    light.position.set(1000, 20, 0)
    light.angle = Math.PI / 2;
    light.penumbra = 0.05;
    light.decay = 2;
    light.distance = 2000;
    light.castShadow = true

    scene.add(light);
    scene.add(light2);

    createGeometry();
    createPlane();
    createStar();
    createSun();


    renderer = new THREE.WebGLRenderer();
    renderer.setSize(window.innerWidth, window.innerHeight);
    document.body.appendChild(renderer.domElement);

    let up = document.getElementById("up");
    up.addEventListener("click", turnAndUp)


}
let mainloop = () => {
    thetaSun += 0.002;
    light.position.y = sun.position.y = Math.sin(thetaSun) * RADIUS_SUN;
    light.position.x = sun.position.x = Math.cos(thetaSun) * RADIUS_SUN;

    camera.lookAt(lookAt);

    if (cube.position.y > yEndPositionCube) {
        cube.position.y -= AddY


    }

    if (lookAt.y < lookAtEndY) {

        lookAt.y += 0.1

    }
    if (camera.position.y < yCameraEnd) {
        camera.position.y += 0.1

    }

    if (startheta < Endtheta) {
        startheta += AddTheta
        camera.position.x = Math.cos(startheta) * RADIUS;
        camera.position.z = Math.sin(startheta) * RADIUS;



    }
    if (light.position.y > camera.position.y) {
        scene.background = new THREE.Color(0x60c3ff);
        light2.intensity = 1
            // sun.material.color = new THREE.Color(0xfff400)
            // light.color = new THREE.Color(0xffffff)



    } else {
        scene.background = new THREE.Color(0x000066);
        light.color = new THREE.Color(0xffffff)

        if (light2.intensity > 0.2) {
            light2.intensity -= 0.005
                // sun.material.color = new THREE.Color(0xFF0000)



        }
    }
    renderer.render(scene, camera);
    requestAnimationFrame(mainloop);

}
init();
mainloop();